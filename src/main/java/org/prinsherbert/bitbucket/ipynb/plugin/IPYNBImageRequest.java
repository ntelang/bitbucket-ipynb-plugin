package org.prinsherbert.bitbucket.ipynb.plugin;

import com.atlassian.bitbucket.content.ContentService;
import com.atlassian.bitbucket.io.TypeAwareOutputSupplier;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;


import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class IPYNBImageRequest extends HttpServlet {
    private final RepositoryService repositoryService;
    private final ContentService contentService;
    
    class ContentCollector implements TypeAwareOutputSupplier {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        String encoding;

        @Override
        public OutputStream getStream(String encoding) throws IOException {
            this.encoding = encoding;
            return out;
        }
        
        public String getContent() throws Exception {
            return new String(out.toByteArray(), "UTF-8");
        }
    };
    
    class Image {
        public final String contentType;
        public final byte[] data;
        public Image(String ct, byte[] data) { this.data=data; this.contentType = ct; }
    }
    
    public IPYNBImageRequest(RepositoryService repositoryService, ContentService contentService) {
        this.repositoryService = repositoryService;
        this.contentService = contentService;
    }
    
    private void reportError(HttpServletResponse resp, String message) throws ServletException, IOException {
        resp.setContentType("text/json;charset=UTF-8");
        resp.getWriter().write("{'success': false, 'message': '" + message + "'}");
    }
    
    private Image decodeImage(String json_string, int index) throws Exception {
        int image_index = 0;
        try {
            JSONObject json = new JSONObject(json_string);
            if (! json.has("cells"))
                throw new Exception("JSON does not contain cells-field");
            JSONArray cells = json.getJSONArray("cells");
            for(int i = 0; i < cells.length(); ++i)
            {
                if (! cells.getJSONObject(i).has("outputs"))
                    continue;
                JSONArray outputs = cells.getJSONObject(i).getJSONArray("outputs");
                for(int j = 0; j < outputs.length(); ++j)
                {
                    if (! outputs.getJSONObject(j).has("data"))
                        continue;
                    JSONObject data = outputs.getJSONObject(j).getJSONObject("data");
                    for(String datatype : data.keySet())
                    {
                        if(datatype.startsWith("image/"))
                            if (image_index == index)
                                return new Image(datatype, Base64.getDecoder().decode(data.getString(datatype).replace("\n", "")));
                            else
                                ++image_index;
                    }

                }
            }
        } catch (JSONException ex) {
            throw new Exception("Could not understand json: " + ex.getMessage());
        } catch (Exception ex) {
            throw new Exception("Could not decode base64 of this image: " + ex.getMessage());
        }
        throw new Exception("No such image with index " + index + ", only found " + image_index + " images.");
    }
 
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String pathInfo = req.getPathInfo();
        String[] components = pathInfo.split("/");
        
        Set<String> knownParameters = new HashSet<>(Arrays.asList(new String[]{"project", "user", "repo", "commit", "release", "file", "image"}));
        HashMap<String, String> parameters = new HashMap<>();
        
        for(int i=1; i<components.length; i+=2) {
            if (i+1 >= components.length) {
                this.reportError(resp, "Expected value for parameter " + components[i]);
                return;
            }
            if (! knownParameters.contains(components[i])) {
                this.reportError(resp, "Unknown parameter " + components[i] + " expected one of " + knownParameters);
                return;
            }
            if (components[i].equals("file")) {
                String file = components[i+1];
                for(int j=i+2; j<components.length; ++j) {
                    file += "/" + components[j];
                }
                parameters.put("file", file);
                break;
            }
            parameters.put(components[i], components[i + 1]);
        }
        if (! parameters.containsKey("project") && ! parameters.containsKey("user")) { reportError(resp, "Specify project or user"); return; }
        if (parameters.containsKey("project") && parameters.containsKey("user")) { reportError(resp, "Specify either project or user"); return; }
        if (! parameters.containsKey("commit")) { reportError(resp, "Specify commit"); return; }
        if (! parameters.containsKey("file")) { reportError(resp, "Specify commit"); return; }
        if (! parameters.containsKey("image")) { reportError(resp, "Specify image (as a nr)"); return; }
        String slug = parameters.containsKey("project") ? parameters.get("project") : parameters.get("user");
        
        final Repository repository = repositoryService.getBySlug(slug, parameters.get("repo"));
        if (repository == null) { reportError(resp, "Could not find repository " + slug + "/" + parameters.get("repo")); return; }
        ContentCollector content = new ContentCollector();
        
        contentService.streamFile(repository, parameters.get("commit"), parameters.get("file"), content);
        try {
            Image img = decodeImage(content.getContent(), Integer.parseInt(parameters.get("image")));
            resp.setContentType(img.contentType);
            resp.getOutputStream().write(img.data);
        } catch (Exception ex) {
            resp.setContentType("text/html;charset=UTF-8");
            resp.setStatus(500);
            resp.getWriter().write(ex.getMessage());
        }
    }

}
