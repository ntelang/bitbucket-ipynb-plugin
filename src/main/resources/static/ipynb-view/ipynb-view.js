define('ipynb/ipynb-view', [
    'jquery',
    'ipynb/ipynb-renderer'
], function(
    $,
    IPYNBRenderer
) {

    /**
     * Constructs a view that renders a .ipynb file in 3D with a toolbar to toggle between different mesh materials
     *
     * @param $container - container to render the .ipynb file in
     * @param ipynbRawUrl - raw url to the .ipynb file
     * @constructor
     */
    function IPYNBView($container, ipynbRawUrl) {
        this.$view = $(bitbucket.feature.fileContent.ipynb.view());
        this.$container = $container.html(this.$view);
        this.ipynbRenderer = new IPYNBRenderer(this.$view);

        this.ipynbRenderer.load(ipynbRawUrl)
    }
    /**
     * This will be called when the view is destroyed
     */
    IPYNBView.prototype.destroy = function() {
        this.ipynbRenderer.destroy();
        this.ipynbRenderer = null;
    };

    return IPYNBView;
});
